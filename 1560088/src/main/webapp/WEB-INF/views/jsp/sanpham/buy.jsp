<%-- 
    Document   : buy
    Created on : May 27, 2018, 1:58:55 PM
    Author     : danh12345
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    </head>
    <body>
        
        
        <div class="container-fluid">
            <br><br>
            <div class="col-sm-2 navbar-fixed-top" style="height: 900px; background: #dff0d8; padding: 0">

                <div class="list-group" style="height: 100%">
                    <a href="http://localhost:8080/MavenProject/" class="list-group-item active">Trang chủ</a>
                    <a href="http://localhost:8080/MavenProject/buy" class="list-group-item list-group-item-success">Mua hàng</a>
                    <a href="http://localhost:8080/MavenProject/check" class="list-group-item list-group-item-success">Kiểm tra sản phẩm</a>
                    <a href="http://localhost:8080/MavenProject/add" class="list-group-item list-group-item-success">Nhập kho</a>

                </div>
            </div>
            <div class="col-sm-6"  style="position: absolute;left: 30%; width: 50%">
                <h1 class="text-center">MUA SẢN PHẨM</h1>
                <form action="cart" width="50%" method="POST">
                    <div class="form-group">
                        <label>MSSP</label>
                        <select class="form-control" name="mssp">
                            <c:forEach items="${lstSanPham}" var="itemSP">
                                <option><c:out value="${itemSP.mssp}"/></option>
                            </c:forEach>

                        </select>

                    </div>

                    <div class="form-group">
                        <label>Số lượng</label>
                        <input type="number" class="form-control" required name="soluong">
                    </div>
                    <button type="submit" class="btn btn-success">Thêm vào giỏ hàng</button>

                </form>
                
                <c:forEach items="${info}" var="item">
                    <c:if test="${item.SoLuong != null}" />
                    <h1 style="color: red">Sản phẩm chỉ còn ${item.SoLuong} ${item.DonViTinh}</h1>   
                </c:forEach>
            </div>
            
        </div>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
