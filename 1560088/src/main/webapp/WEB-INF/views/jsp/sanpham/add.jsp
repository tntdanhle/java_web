<%-- 
    Document   : them
    Created on : May 24, 2018, 4:47:39 PM
    Author     : danh12345
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    </head>
    <body>
        
        
        
        
        
        <div class="container-fluid">
            <br><br>
                <div class="col-sm-2 navbar-fixed-top" style="height: 900px; background: #dff0d8; padding: 0">

                    <div class="list-group" style="height: 100%">
                        <a href="http://localhost:8080/MavenProject/" class="list-group-item active">Trang chủ</a>
                        <a href="http://localhost:8080/MavenProject/buy" class="list-group-item list-group-item-success">Mua hàng</a>
                        <a href="http://localhost:8080/MavenProject/check" class="list-group-item list-group-item-success">Kiểm tra sản phẩm</a>
                        <a href="http://localhost:8080/MavenProject/add" class="list-group-item list-group-item-success">Nhập kho</a>
                        
                    </div>
                </div>
            <div class="col-sm-5 pull-right">
                <h1 class="text-center">SẢN PHẨM CÓ SẲNG</h1>
                <form action="updateqtt" width="50%" method="POST">
                    <div class="form-group">
                        <label>MSSP</label>
                        <select class="form-control" name="mssp">
                            <c:forEach items="${lstSanPham}" var="itemSP">
                                
                                <option value="<c:out value='${itemSP.mssp}'/>"><c:out value="${itemSP.mssp}"/></option>
                            </c:forEach>

                        </select>

                    </div>
                      
                            
                    <div class="form-group">
                        <label>Số lượng</label>
                        <input type="number" class="form-control" required name="soluong">
                    </div>
                    <button type="submit" class="btn btn-success">Nhập hàng</button>

                </form>
            </div>
            
            
            <div class="col-sm-5 pull-right">
                <h1 class="text-center">SẢN PHẨM MỚI</h1>
                <form action="adding" width="50%" method="POST">
                    <div class="form-group">
                        <label>MSSP</label>
                        <input type="text" class="form-control" required name="mssp">

                    </div>
                            
                    <div class="form-group">
                        <label>Tên sản phẩm</label>
                        <input type="text" class="form-control" required name="ten">
                    </div>
                            
                    <div class="form-group">
                        <label>Số lượng</label>
                        <input type="number" class="form-control" required name="soluong">
                    </div>
                    
                    <div class="form-group">
                        <label>Đơn vị tính</label>
                        <input type="text" class="form-control" required name="donvitinh">
                    </div>
                    
                    <div class="form-group">
                        <label>Giá</label>
                        <input type="number" class="form-control" required name="gia">
                    </div>
                    <button type="submit" class="btn btn-success">Thêm vào kho</button>

                </form>
            </div>
        </div>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
    </body>
</html>
