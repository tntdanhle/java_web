<%-- 
    Document   : buying
    Created on : May 27, 2018, 3:03:15 PM
    Author     : danh12345
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="modules.SanPham"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    </head>
    <body>
        <c:set var="sum" value="0"></c:set>
        <% ArrayList<SanPham> lstSP = (ArrayList<SanPham>)session.getAttribute("lstSP"); %>
        <div class="container-fluid">
            <div class="row">
                <br><br>
                <div class="col-sm-2 navbar-fixed-top" style="height: 900px; background: #dff0d8; padding: 0">

                    <div class="list-group">
                        <a href="http://localhost:8080/MavenProject/" class="list-group-item active">Trang chủ</a>
                        <a href="http://localhost:8080/MavenProject/buy" class="list-group-item list-group-item-success">Mua hàng</a>
                        <a href="http://localhost:8080/MavenProject/check" class="list-group-item list-group-item-success">Kiểm tra sản phẩm</a>
                        <a href="http://localhost:8080/MavenProject/add" class="list-group-item list-group-item-success">Nhập kho</a>
                        
                    </div>
                </div>
                <div class="col-sm-10 pull-right">
                    
                    <h1>Danh sach giỏ hàng</h1>
                    
                    <br>
                    <table class="table table-bordered">
                        <tr width="">
                            <th>MSSP</th>
                            <th>Tên sản phẩm</th>
                            <th>Số lượng mua</th>
                            <th>Giá</th>
                            <th>Thành tiền</th>

                        </tr>
                        <c:forEach var="row" begin="0" items="<%= lstSP %>">
                            <tr>
                                <td>
                                    <c:out value="${row.getMSSP()}" />
                                </td>
                                <td>
                                    <c:out value="${row.getTen()}" />
                                </td>
                                <td>
                                    <c:out value="${row.getSoLuong()}" />
                                </td>
                                <td>
                                    <c:out value="${row.getGia()}" />
                                </td>
                                <td>
                                    <c:out value="${Integer.parseInt(row.getGia())* Integer.parseInt(row.getSoLuong())}" />
                                    <c:set var="sum" value="${sum + Integer.parseInt(row.getGia())* Integer.parseInt(row.getSoLuong())}" />
                                </td>

                            </tr>
                        </c:forEach>   
                    </table>
                    <h2>Tổng tiền : <c:out value="${sum}" /></h2>
                   <br>
                   <form action="buy" method="GET" class="pull-left">
                        <input class="btn btn-primary" type="submit" value="Mua tiếp">
                    </form>

                    <form action="payment" method="GET" class="pull-right">
                        <input class="btn btn-primary" type="submit" value="Thanh toán">
                    </form>
                   
                </div>
            </div>        
        </div>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
