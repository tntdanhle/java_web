/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Repo.SanPhamRepo;
import modules.SanPham;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author tu pham
 */
@Controller
@RequestMapping("/")
public class HomeController {
    @Autowired
    DataSource dataSource;
    ArrayList<SanPham> lstSP = new ArrayList<SanPham>();
    HttpSession mySession = null;
    //private PersonService personService;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String list(Map map, Model m) {
        SanPhamRepo sp = new SanPhamRepo(dataSource);
        List<SanPham> list = sp.getListSanPham();
        
        m.addAttribute("lstSanPham", list);
        //map.put("personList", personService.list());
        return "sanpham/index";
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String AddPage(Model model) {
        SanPhamRepo spBuy = new SanPhamRepo(dataSource);
        List<SanPham> list = spBuy.getListSanPham();
        
        model.addAttribute("lstSanPham", list);
        return "sanpham/add";
    }
    
    @RequestMapping(value = "/adding", method = RequestMethod.POST)
    public RedirectView Adding(SanPham sp, HttpServletRequest req, HttpServletResponse res) {
        SanPhamRepo sprp = new SanPhamRepo(dataSource);
        
        sp.setMSSP(req.getParameter("mssp"));
        sp.setTen(req.getParameter("ten"));
        sp.setSoLuong(Integer.parseInt(req.getParameter("soluong")));
        sp.setDonViTinh(req.getParameter("donvitinh"));
        sp.setGia(Integer.parseInt(req.getParameter("gia")));
        
        boolean ch = sprp.Add(sp);
        
        RedirectView rv = new RedirectView();
        rv.setContextRelative(true);
        rv.setUrl("/");
        return rv;
    }
    
    @RequestMapping(value = "/updateqtt", method = RequestMethod.POST)
    public RedirectView UpdateQTT(SanPham sp, HttpServletRequest req, HttpServletResponse res) {
        SanPhamRepo sprp = new SanPhamRepo(dataSource);
       
        
        boolean ch = sprp.UpdateQTT(req.getParameter("mssp"),Integer.parseInt(req.getParameter("soluong")));
        
        RedirectView rv = new RedirectView();
        rv.setContextRelative(true);
        rv.setUrl("/");
        return rv;
    }
    
    @RequestMapping(value = "/buy", method = RequestMethod.GET)
    public String Buy(SanPham sp, HttpServletRequest req, HttpServletResponse res, Model model) {
        SanPhamRepo spBuy = new SanPhamRepo(dataSource);
        List<SanPham> list = spBuy.getListSanPham();
        
        model.addAttribute("lstSanPham", list);
        return "sanpham/buy";
    }
    
    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    public String Cart(SanPham sp, HttpServletRequest req, HttpServletResponse res, Model model, Model model2) {
        SanPhamRepo sprpCart = new SanPhamRepo(dataSource);
        
        
        RedirectView rv = new RedirectView();
        
        rv.setContextRelative(true);
        
        SanPham spCart = new SanPham();
        spCart.setMSSP(req.getParameter("mssp"));
        
        spCart.setSoLuong(Integer.parseInt(req.getParameter("soluong")));
        
        List<SanPham> aSanPham = sprpCart.getSanPhamByMssp(spCart.getMSSP());
        
        
        Iterator _terator = aSanPham.iterator();
        int gia = (int)((Map)_terator.next()).get("Gia");
        _terator = aSanPham.iterator();
        int soluongton = (int)((Map)_terator.next()).get("SoLuong");
        
        _terator = aSanPham.iterator();
        String ten = (String)((Map)_terator.next()).get("Ten");
        if(soluongton < spCart.getSoLuong()){
            SanPhamRepo _sp = new SanPhamRepo(dataSource);
            List<SanPham> list = _sp.getListSanPham();

            model.addAttribute("lstSanPham", list);
            
            model.addAttribute("info", aSanPham);
            rv.setUrl("buy");
            return "sanpham/buy";
        }
        
        spCart.setGia(gia);
        spCart.setTen(ten);
        lstSP.add(spCart);
        
        mySession = req.getSession();
        
        mySession.setAttribute("lstSP", lstSP);
        
        return "sanpham/buying";
    }
    
    @RequestMapping(value = "/buying", method = RequestMethod.GET)
    public String Buying(SanPham sp, HttpServletRequest req, HttpServletResponse res, Model model) {
        
       
        return "sanpham/buying";
    }
    
    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public RedirectView payment(SanPham sp, HttpServletRequest req, HttpServletResponse res, Model model) {
        SanPhamRepo spBuy = new SanPhamRepo(dataSource);
        RedirectView rv = new RedirectView();
        for(int i=0;i<lstSP.size();i++){
            spBuy.Update((SanPham)lstSP.get(i));
        }
        lstSP.clear();
        mySession = null;
        rv.setContextRelative(true);
        rv.setUrl("/");
        return rv;
    }
    
    
    
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public String check(SanPham sp, HttpServletRequest req, HttpServletResponse res, Model model) {
        SanPhamRepo spBuy = new SanPhamRepo(dataSource);
        List<SanPham> list = spBuy.getListSanPham();
        
        model.addAttribute("lstSanPham", list);
        return "sanpham/checkQtt";
    }
}
