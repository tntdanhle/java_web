<%-- 
    Document   : list
    Created on : May 17, 2018, 7:39:29 AM
    Author     : tu pham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>


        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <br><br>
                <div class="col-sm-2 navbar-fixed-top" style="height: 900px; background: #dff0d8; padding: 0">

                    <div class="list-group" style="height: 100%">
                        <a href="http://localhost:8080/MavenProject/" class="list-group-item active">Trang chủ</a>
                        <a href="http://localhost:8080/MavenProject/buy" class="list-group-item list-group-item-success">Mua hàng</a>
                        <a href="http://localhost:8080/MavenProject/check" class="list-group-item list-group-item-success">Kiểm tra sản phẩm</a>
                        <a href="http://localhost:8080/MavenProject/add" class="list-group-item list-group-item-success">Nhập kho</a>
                        
                    </div>
                </div>
                <div class="col-sm-10 pull-right">
                    
                    <h1 class="text-center">DANH SÁCH SẢN PHẦM</h1>
                    
                    <br>
                    <table class="table table-bordered">
                        <tr width="">
                            <th>MSSP</th>
                            <th>Tên sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Đơn vị tính</th>
                            <th>Giá</th>

                        </tr>
                        <c:forEach items="${lstSanPham}" var="itemSP">
                            <tr>
                                <td>${itemSP.mssp}</td>
                                <td>${itemSP.ten}</td>
                                <td>${itemSP.SoLuong}</td>
                                <td>${itemSP.DonViTinh}</td>
                                <td>${itemSP.Gia}</td>

                            </tr>   
                        </c:forEach>    
                    </table>
                   
                </div>
            </div>        
        </div>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
