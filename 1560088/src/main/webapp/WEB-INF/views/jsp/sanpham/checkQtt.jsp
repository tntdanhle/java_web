<%-- 
    Document   : checkQtt
    Created on : Jun 1, 2018, 3:40:43 PM
    Author     : danh12345
--%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    </head>
    <body>
        <div class="container-fluid">
            <br><br>
            <div class="col-sm-2 navbar-fixed-top" style="height: 900px; background: #dff0d8; padding: 0">

                <div class="list-group" style="height: 100%">
                    <a href="http://localhost:8080/MavenProject/" class="list-group-item active">Trang chủ</a>
                    <a href="http://localhost:8080/MavenProject/buy" class="list-group-item list-group-item-success">Mua hàng</a>
                    <a href="http://localhost:8080/MavenProject/check" class="list-group-item list-group-item-success">Kiểm tra sản phẩm</a>
                    <a href="http://localhost:8080/MavenProject/add" class="list-group-item list-group-item-success">Nhập kho</a>

                </div>
            </div>
            <div class="col-sm-6" style="position: absolute;left: 30%; width: 50%">
                <h1 class="text-center">KIỂM TRA SỐ LƯỢNG TỒN</h1>
                <form action="" width="50%" method="GET">
                    <div class="form-group">
                        <label>MSSP</label>
                        <select class="form-control" name="mssp" >
                            <option>---Click Chọn MSSP---</option>
                            <c:forEach items="${lstSanPham}" var="itemSP">
                                <option><c:out value="${itemSP.mssp}"/></option>
                            </c:forEach>

                        </select>

                    </div>

                    <button type="submit" class="btn btn-success">Kiểm tra</button>
                </form>
                <c:set var="resoul" value="0" />
                <sql:setDataSource var="data" driver="com.mysql.jdbc.Driver"
                                       url="jdbc:mysql://localhost:3306/quanlysanpham"
                                       user="root"
                                       password=""/>
                    <sql:query var="resoul" dataSource="${data}">
                        SELECT * from sanpham where mssp = ?;
                        <sql:param value="${param.mssp}"/>

                    </sql:query>

                        <c:forEach var="row" begin="0" items="${resoul.rows}">
                            <h2>Sản phâm "<c:out value="${row.Ten}" />" còn <c:out value="${row.SoLuong}" /> (<c:out value="${row.DonViTinh}" />)</h2>
                            <c:choose>
                                <c:when test = '${row.SoLuong != 0}'>
                                    <h1 style="color: green">Còn hàng </h1>
                                </c:when>
                                <c:otherwise>
                                    <h1 style="color: red">HẾT hàng </h1>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </div>
        </div>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        
        
    </body>
</html>
