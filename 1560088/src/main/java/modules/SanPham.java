/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modules;

import java.io.Serializable;

/**
 *
 * @author tu pham
 */
public class SanPham implements Serializable{
    private int _Gia, SoLuong;

    public int getGia() {
        return _Gia;
    }

    public void setGia(int _Gia) {
        this._Gia = _Gia;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }

    public String getMSSP() {
        return _MSSP;
    }

    public void setMSSP(String _MSSP) {
        this._MSSP = _MSSP;
    }

    public String getTen() {
        return _Ten;
    }

    public void setTen(String _Ten) {
        this._Ten = _Ten;
    }

    public String getDonViTinh() {
        return _DonViTinh;
    }

    public void setDonViTinh(String _DonViTinh) {
        this._DonViTinh = _DonViTinh;
    }
    private String _MSSP, _Ten, _DonViTinh;

    public SanPham() {
    }

    
    
}
