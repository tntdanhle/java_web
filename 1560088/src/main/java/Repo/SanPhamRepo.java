/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repo;

import modules.SanPham;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.ui.Model;
import java.sql.Types;
/**
 *
 * @author danh12345
 */

public class SanPhamRepo {
    @Autowired
    protected DataSource _dataSource;

    public SanPhamRepo(DataSource dataSource) {
        this._dataSource = dataSource;
    }
    
    public  List<SanPham> getListSanPham() {
        
        List userList = new ArrayList();
        String sql = "select * from sanpham";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(this._dataSource);
        userList = jdbcTemplate.queryForList(sql);
        return userList;
    }
    @Autowired
    public boolean Add(SanPham sp) {
        
        
        //List userList = new ArrayList();
        String sql = "insert into sanpham(mssp, Ten, SoLuong, DonViTinh, Gia) values(?, ?, ?, ?, ?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(this._dataSource);
        Object[] params = new Object[] { sp.getMSSP(), sp.getTen(), sp.getSoLuong(), sp.getDonViTinh(), sp.getGia() };
        int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.INTEGER };
        return jdbcTemplate.update(sql, params, types)>0;
        
    }
    
    public boolean Update(SanPham sp) {
        
        
        //List userList = new ArrayList();
        String sql = "Update sanpham set SoLuong = SoLuong - ? where mssp = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(this._dataSource);
        Object[] params = new Object[] {sp.getSoLuong(), sp.getMSSP() };
        int[] types = new int[] {Types.INTEGER, Types.VARCHAR};
        return jdbcTemplate.update(sql, params, types)>0;
        
    }
    
    public boolean UpdateQTT(String mssp, int qtt) {
        
        
        //List userList = new ArrayList();
        String sql = "Update sanpham set SoLuong = ? where mssp = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(this._dataSource);
        Object[] params = new Object[] {qtt, mssp};
        int[] types = new int[] {Types.INTEGER, Types.VARCHAR};
        return jdbcTemplate.update(sql, params, types)>0;
        
    }
    
    public  List<SanPham> getSanPhamByMssp(String mssp) {
        List userList = new ArrayList();
        String sql = "select * from sanpham where mssp = '"+mssp+"'";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(this._dataSource);
        Object[] params = new Object[] {mssp };
        int[] types = new int[] {Types.VARCHAR};
        userList = jdbcTemplate.queryForList(sql);
        return userList;
    }
    
    
}